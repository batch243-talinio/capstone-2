const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type : Boolean,
        default : false
    },
    credit: {
        type: Number,
        default: 0
    },
    orders: [{
        products: [
            {
                productName : {
                    type : String,
                    required : [true, "Product name is required"]
                },
                quantity : {
                    type : Number,
                    required : [true, "Quantity is required"]
                },
            },
        ],
        totalAmount : {
            type : Number,
            required: [true, "Amount is required!"]
        },
        purchasedOn : {
            type : Date,
            default : new Date()
        },
    }]
    
})

module.exports = mongoose.model("User", userSchema)


/*Inserting userInfo to PRODUCTS
Inserting checked-out product/s to USER

module.exports.productQuantity = (request, response) => {
    let userData = auth.decode(request.headers.authorization);
    let productId = request.params.productId;

    Product.findById(productId).then(result => { 
        if (result.quantity < request.body.products.quantity){
            return response.send(`You ordered more than the current available stock. Please try again.`)
        }
        next();
    }).catch(error => `error here ${false}`)
}





module.exports.checkOutProduct = async (request, response) => {

    let userData = auth.decode(request.headers.authorization);
    let productId = request.params.productId;

    let pName;
    let pQuantity;
    Product.findById(productId).then(result => {
        pName = result.name
        pQuantity = result.quantity
    })
    let update = {
        orders:[{
            products: {
                name: pName,
                quantity: request.body.products.quantity,
            },
            totalAmount: pQuantity * request.body.products.quantity,
        }]
    }

    let quantityProduct = {
        quantity: pQuantity - request.body.products.quantity,
        orders: {
            orderId: userData.id
        }
    }

    if(!userData.isAdmin){ }

    // User id added tp Product.orders
    let updateProduct = await Product.findById(productId).then(result => {
        result.orders.push(update)
        

        return result.save().then(success => {
            return true;
        }).catch(error => false)
            
    }).catch(error => response.send(false))

    // Oredered product added to User.orders
    let updateUser = await User.findById(userData.id).then(result =>{
        result.orders.push(quantityProduct)

        return result.save().then(success => {
            return true;
        }).catch(error =>  false)

    }).catch(error => response.send(false))


    // Check if all changes are made or successful
        return (updateProduct && updateUser) ? response.send("You are now Enrolled!") : response.send("We encountered a problem in your enrollment. Please try again!")

    }
*/