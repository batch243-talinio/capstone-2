const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product name is required!"]
    },

    description: {
        type: String,
        required: [true, "Products description is required!"]
    },

    quantity: {
        type: Number,
        required: [true, "Products quantity is required!"]
    },

    price : {
        type : Number,
        required: [true, "Products price is required!"]
    },

    isActive : {
        type : Boolean,
        default : true
    },

    createdOn : {
            type : Date,
            default : new Date()
        },

    orders: [{
        orderId: {
        	type: String,
        	required: [true, "Order Id is required!"]
    	},

    }]
    
})

module.exports = mongoose.model("Product", productSchema)