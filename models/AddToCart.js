const mongoose = require('mongoose')

const cartSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, "User Id is required!"]
    },

    productId: {
        type: String,
        required: [true, "Product id is required!"]
    },

    isActive: {
        type : Boolean,
        default : true
    },

    createdOn : {
        type : Date,
        default : new Date()
    },
    
})

module.exports = mongoose.model("Cart", cartSchema)