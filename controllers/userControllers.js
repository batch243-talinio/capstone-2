const ATCart = require("../models/AddToCart");
const Product = require("../models/Product");
const User = require("../models/User");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// Checking if Email from the user is already taken or not.

module.exports.checkEmailExists = (request, response, next) => {
	return User.find({email: request.body.email}).then(result => {
		let message = `${request.email}`;
		if(result.length > 0){
			message = `Email already taken. Please use other email.`;
			return response.send(message);

		}else{
			next();
		}
	})
}

// Register the User
module.exports.registerUser = (request, response) =>{

	let newUser = new User({
		email: 		request.body.email,
		password: 	bcrypt.hashSync(request.body.password, 10),
		credit: 		request.body.credit

	})

	return newUser.save().then(user => {
		response.send(`Congratulations. You are now successfully Registered!`)
	}).catch(error => {
		console.log(`Sorry. An error occured during the registration. Please try again.`)
	})
}


// Log-in User with Authentication
module.exports.loginUser = (request, response) => {
	
	return User.findOne({email : request.body.email})
	.then(result => {

		if(result == null){
			response.send(`The email you entered doesn't exist. Please try again.`)
		}else{
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return User.findOne({email : request.body.email})
					.then(result => {response.send(`Token ${auth.createAccessToken(result)}`)})

			}else{
				return response.send(`Incorrect password. Please try again.`)
			}
		}
	})
}


// Update role (admin to not-admin, vice-versa)
module.exports.updateUserRole = (request, response) => {

	let userData = auth.decode(request.headers.authorization);

	let idToBeUpdated = request.params.userId;

	if(userData.isAdmin){
		return User.findById(idToBeUpdated).then(result => {

			let update = {
				isAdmin : !result.isAdmin
			}

			return User.findByIdAndUpdate(idToBeUpdated, update, {new: true}).then(document => {
					result.password = "Confidential."
					response.send(`User role updated! \n ${document}`);

				}).catch(err => response.send(err))
		
		}).catch(err => response.send(err));

	}else{
		return response.send("You don't have access to this page!")
	}
}


// Check order if it's more than the stock available 
module.exports.productQuantity = (request, response, next) => {
    let userData = auth.decode(request.headers.authorization);
    let productId = request.params.productId;

    Product.findById(productId).then(result => { 
        if (result.quantity < request.body.products.quantity){
            return response.send(`You ordered more than the current available stock. Please try again.`)
        }
        next();
    }).catch(error => `error here ${false}`)
}


// Check-out / Buy Product (User only)
module.exports.checkOutProduct = async (request, response, next) => {
	let userData = auth.decode(request.headers.authorization);
	let productId = request.params.productId;

	if(userData.isAdmin){ return response.send("You don't have access to this page!")}

	// Inserting userInfo to PRODUCTS
	let isProductUpdated = await Product.findById(productId).then(result => {
		result.orders.push({
			orderId: userData.id
		})
		result.quantity -= request.body.products.quantity

		return result.save().then(success => true ).catch(error => false)
	}).catch(error => response.send(false))

	let prodName; let prodPrice;
	let details = await Product.findById(productId).then(result => { prodName = result.name; prodPrice = result.price; }).catch(error => `error here ${false}`)

	// Inserting checked-out product/s to USER
	let isUserUpdated = await User.findById(userData.id).then(result =>{
		result.credit = result.credit - (prodPrice * request.body.products.quantity)
		result.orders.push({
			products: {
				productName: prodName,
            	quantity: request.body.products.quantity
			},
			totalAmount: prodPrice * request.body.products.quantity
		})

		return result.save().then(success => true ).catch(error =>  false)
	}).catch(error => response.send(false))


	// Check if all changes are made or successful
	return (isProductUpdated && isUserUpdated) ? next() : response.send(`An error occured. Please try again.`)
}

module.exports.baughtProduct = (request, response) => {
	return response.send(`!!Transaction Complete!! \nProduct checked-out successfully. Please come again.`);
}

// Remove added to cart products
module.exports.removeCart = (request, response) => {
    let userData = auth.decode(request.headers.authorization);
    let productIdz = request.params.productId;
    
    ATCart.findOne({userId: userData.id, productId: productIdz}).then(result => {
    	const cartId = result.id;
    	let update = {
				isActive : false
			}

		return ATCart.findByIdAndUpdate(cartId, update, {new: true}).then(document => {
			response.send(`!!Transaction Complete!! \nProduct checked-out successfully. Please come again.`);

		}).catch(err => response.send(err))
    }).catch(error => `error here ${false}`)
    

        
}

/*
else if(result.credit < (prodPrice * request.body.products.quantity)){
        	return response.send(`You don't have enough credit/money. Please utang first.`)
        }
*/

//return (updateProduct && updateUser) ? response.send("You are now Enrolled!") : response.send("We encountered a problem in your enrollment. Please try again!")
//return User.findByIdAndUpdate(userData.id, update, {new: true}).then(document => response.send(true) ).catch(err => response.send(` HERES THE ERROR one \n ${err}`))


/*
let userData = auth.decode(request.headers.authorization);

	return userData.isAdmin ? response.send("You don't have access to this page!") : Cart.find({userId : userData.id})
		.then(result => response.send(result)).catch(error => response.send(error))



module.exports.checkOutProduct = (request, response) => {

	let userData = auth.decode(request.headers.authorization);
	let productId = request.params.productId;

	// let cartProduct = () => ATCart.

	//return response.send("HELLO" + request.body.products.quantity)

	if(!userData.isAdmin && userData != ''){
		return Product.findById(productId).then(result => {

		if(result.quantity < request.body.quantity) { return response.send(`You ordered more than the current available stock. Please try again.`)}

			let update = {
				orders:[{
					products: {
						name: result.name,
	                	quantity: request.body.products.quantity,
					},
					totalAmount: result.quantity * request.body.products.quantity,
				}]

			}
			let quantityProduct = {
				quantity: result.quantity - request.body.quantity,
				orders: {
					orderId: userData.id
				}

			}
			//test w/o this part
			//Product.findByIdAndUpdate(productId, quantityProduct, {new: true})

			return User.findByIdAndUpdate(userData.id, update, {new: true}).then(document => {
					response.send(`!!Transaction Complete!! \nProduct checked-out successfully. Please come again.${document}`);
				}).catch(err => response.send(` HERES THE ERROR two \n ${err}`))
		
		}).catch(err => response.send(` HERES THE ERROR ONE \n ${err}`));

	}

	return response.send("You don't have access to this page!")
	

}
*/




/*
module.exports.checkOutProduct = async (request, response) => {
	let userData = auth.decode(request.headers.authorization);
	let productId = request.params.productId;
	let prodName;
	let quantityP;

	if(userData.isAdmin){ return response.send("You don't have access to this page!")}

	return Product.findById(productId).then(result => {
	
		if(result.quantity < request.body.products.quantity) { return response.send(`You ordered more than the current available stock. Please try again.`)}
		prodName = result.name
		quantity = result.quantity
	}).catch(err => response.send(` HERES THE ERROR ONE \n ${err}`));

	let updateProduct = await User.findById(userData.id).then(result => {
        	result.orders.push({
        		products: {
					productName: prodName,
                	quantity: request.body.products.quantity,
				},
				totalAmount: quantityP * request.body.products.quantity,
        	})
        
        	return result.save().then(success => {
           		return response.send(true);
        	}).catch(error => false)
            
    	}).catch(error => response.send(` HERES THE ERROR two \n ${err}`))

	return updateProduct ? `success` : `failed`
}
*/