const express = require("express");
const router = express.Router();
const auth = require("../auth")

const userController = require("../controllers/userControllers")

	// Route for registration
		router.post("/register", userController.checkEmailExists, userController.registerUser);

	// Route for log-in
		router.post("/login", userController.loginUser);

	// Update user role (admin)
		router.patch("/updateRole/:userId", auth.verify, userController.updateUserRole);

// Check-out/buy product (user)
		router.put("/checkOut/:productId", auth.verify, userController.productQuantity, userController.checkOutProduct, userController.baughtProduct);

	// Check-out/buy product from Cart(user)
		router.put("/checkOut/:productId", auth.verify, userController.productQuantity, userController.checkOutProduct, userController.removeCart);

//, userController.removeCart
module.exports = router;