const jwt = require("jsonwebtoken");
const secret = "Capstone2";

module.exports.createAccessToken = (result) => {
	
	const data = {
		id: result._id,
		email: result.email,
		isAdmin: result.isAdmin

	}
	return jwt.sign(data, secret, {});
}

// Token Verification
module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;

	if(token !== undefined){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error) => {
			if(error){
				return response.send(`User account doesn't exist! Please log-in first.`)
			}else{
				next();
			}
		})

	}else{
		return response.send(`Action failed! Please log-in first.`)
	}


}


// Token Decryption
module.exports.decode = (token) => {
	if(token === undefined){
		return null;

	}else{
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return null;

			}else{

				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
}



