// Set-up Dependencies
const express = require("express");
const mongoose = require("mongoose");
//const cors = require('cors');
const app = express();


// allows access to routes defined within
	const userRoutes = require("./routes/userRoutes");
	const productRoutes = require("./routes/productRoutes");
	const addToCart = require("./routes/cartRoutes");



// MIDDLEWARES
	//app.use(cors());
	app.use(express());
	app.use(express.json());
	app.use(express.urlencoded({extended:true}));

// ROUTES
	app.use("/users", userRoutes);
	app.use("/products", productRoutes);
	app.use("/users/products", addToCart);


// Database connection
	mongoose.set('strictQuery', true); // [MONGOOSE] DeprecationWarning
	
	mongoose.connect("mongodb+srv://admin:admin@zuittbatch243talinio.zbprtxo.mongodb.net/Capstone_Two?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);


	mongoose.connection.on("error", console.error.bind(console, "connection error"));
	mongoose.connection.once('open',() => console.log("Now connected to MongoDB Atlas"));

	app.listen(process.env.PORT || 4000, () =>{
		console.log(`API is now Online on port ${process.env.PORT || 4000}`);
	})